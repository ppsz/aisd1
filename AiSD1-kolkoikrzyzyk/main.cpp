#include "game.h"

int main()
{
	int menu_select;
	for (;;)
	{
		std::cout
			<< "|---------------------------------|\n"
			<< "|           Menu Glowne           |\n"
			<< "|---------------------------------|\n"
			<< "| 1: Nowa Gra - Gracz vs Gracz    |\n"
			<< "| 2: Nowa Gra - Gracz vs Komputer |\n"
			<< "|                                 |\n"
			<< "| 0: Wyjdz z programu             |\n"
			<< "|---------------------------------|\n";

		menu_select = GetValue("   Wybierz pozycje menu: ", 0, 2, false);

		switch (menu_select)
		{
		case 1:
			g_vs_computer = false;
			GameProgress();
			break;
		case 2:
			g_vs_computer = true;
			GameProgress();
			break;
		case 0:
			return 0;
			break;
		default:
			return -1;
		}
	}
	return 0;
}