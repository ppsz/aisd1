#include "game.h"

Board g_gameboard[9];
SIGN g_current_player;
CURRENTFIELD g_current_field;
int g_number_of_moves;
bool g_vs_computer;
GAMESTATE g_gamestate = GS_BEGIN;
PLAYER g_computer_move = P_HUMAN;

bool StartGame()
{
	if(g_gamestate != GS_BEGIN) return false;
	
	//---losowy rozpoczynajacy gracz---
	std::random_device rd;
	std::uniform_int_distribution<short> dist(0, 1);
	g_current_player = (dist(rd) == 0? SGN_CIRCLE : SGN_CROSS);

	//---zerowanie planszy---
	for (int i = 0; i < 9; ++i)
		for (int j = 0; j < 3; ++j)
			for (int k = 0; k < 3; ++k)
				g_gameboard[i].board_element[j][k] = SGN_EMPTY;

	//---Ustawienie poczatkowej planszy i stanu gry
	g_current_field = CF_CENTER;
	g_gamestate = GS_PLAY;
	g_computer_move = P_HUMAN;
	g_number_of_moves = 1;

	return true;
}
//----------------------------------------------------------------------------------------
bool DrawBoard()
{
	if (g_gamestate == GS_BEGIN) return false;

	system("cls");

	//---rysowanie planszy---
	std::cout << "        =============================\n";
	for (int h = 0, current = 0; h < 3; ++h)
	{
		for (int i = 0; i < 3; ++i, ++current)
		{
			for (int j = 0; j < 3; ++j, ++current)
			{
				if (j == 0) 
				{
						if (h * 3 + j == g_current_field)
							std::cout << (g_current_player==SGN_CIRCLE? "        O> " : "        X> ");
						else
							std::cout << "        ][ ";
				}
				for (int k = 0; k < 3; ++k)
				{
					//---jezeli puste pole wypisanie pustego pola lub numeru pola, jezeli zapelnione wypisanie znaczka
					if (g_gameboard[h * 3 + j].board_element[i][k] == SGN_EMPTY && g_gamestate == GS_PLAY)
					{
						if (h * 3 + j == g_current_field)
							std::cout << ((i * 3 + k + 1)) << (k < 2 ? "|" : "");
						else
							std::cout << " " << (k < 2 ? "|" : "");
					}
					else
						std::cout << static_cast<char>(g_gameboard[h * 3 + j].board_element[i][k]) << (k < 2 ? "|" : "");
				}
				//---oznaczenie aktualnego pola
				if (h * 3 + j + 1 == g_current_field && g_current_field != 0 && g_current_field != 3 && g_current_field != 6) 
					std::cout << (g_current_player == SGN_CIRCLE ? " O> " : " X> ");
				else if (h * 3 + j == g_current_field)
					std::cout << (g_current_player == SGN_CIRCLE ? " <O " : " <X ");
				else
					std::cout << " ][ ";
			}
			std::cout << (i < 2 ? "\n        ][ ---|- ][ -|-|- ][ -|--- ][\n" : "\n"); // srodek kazdej planszy
		}
		std::cout << "        =============================\n";
	}

	//sprawdzenie stanu gry i wyswietlenie odpowiedniej wiadomosci
	switch (g_gamestate)
	{
	case GS_PLAY:
		std::cout << "Tura gracza stawiajacego " << (g_current_player == SGN_CIRCLE ? "kolko" : "krzyzyk") << ".\n";
		break;
	case GS_END_WIN:
		std::cout << "Zwyciezyl gracz stawiajacy " << (g_current_player == SGN_CIRCLE ? "kolka" : "krzyzyki") << "!\n";
		std::cout << "Gra zajela ilosc ruchow: " << g_number_of_moves << "\n";
		break;
	case GS_END_DRAW:
		std::cout << "Remis!\n";
		break;
	}
}
//----------------------------------------------------------------------------------------
bool PlaceSign(int field_number)
{
	if (g_gamestate != GS_PLAY) return false;
	
	//---stala posiadajace wszystkie zwyciezajace kombinacje (w formie {Y,X})
	const int WINNING_LINES[8][3][2] = {
		{ { 0, 0 },{ 0, 1 },{ 0, 2 } },	// g�rna pozioma
		{ { 1, 0 },{ 1, 1 },{ 1, 2 } },	// �rodkowa pozioma
		{ { 2, 0 },{ 2, 1 },{ 2, 2 } },	// dolna pozioma
		{ { 0, 0 },{ 1, 0 },{ 2, 0 } },	// lewa pionowa
		{ { 0, 1 },{ 1, 1 },{ 2, 1 } },	// �rodkowa pionowa
		{ { 0, 2 },{ 1, 2 },{ 2, 2 } },	// prawa pionowa
		{ { 0, 0 },{ 1, 1 },{ 2, 2 } },	// przek�tna lewo>prawo
		{ { 2, 0 },{ 1, 1 },{ 0, 2 } } };	// przek�tna prawo<lewo

	//---przeksztalcenie numeru pola na wspolrzedne X i Y
	int y_coordinate = (field_number - 1) / 3;
	int x_coordinate = (field_number - 1) % 3;

	//---warunek sprawdzajacy czy pole jest puste (nie mozna dodac dwa razy tego samego znaku w jedno pole)
	if (g_gameboard[g_current_field].board_element[y_coordinate][x_coordinate] == SGN_EMPTY)
		g_gameboard[g_current_field].board_element[y_coordinate][x_coordinate] = g_current_player;
	else
		return false;

	//---sprawdzanie czy ktos zwyciezyl
	for (int i = 0; i < 8; ++i)
	{
		int same_fields_counter = 0;
		SIGN field = SGN_EMPTY;
		SIGN same_field = SGN_EMPTY;

		for (int j = 0; j < 3; ++j)
		{
			field = g_gameboard[g_current_field].board_element[WINNING_LINES[i][j][0]][WINNING_LINES[i][j][1]];
			if (field == same_field)
			{
				++same_fields_counter;
			}
			else
			{
				same_field = field;
				same_fields_counter = 1;
			}
		}

		//---jezeli zajete sa 3 nie puste pola w jednej linii-> ustaw stan zwyciestwa
		if (same_fields_counter == 3 && same_field != SGN_EMPTY)
		{
			g_gamestate = GS_END_WIN;
			return true;
		}
	}

	//---Sprawdzanie warunku remisu(?)
	int filled_fields = 0;
	for (int h = 0; h < 3; ++h)
		for (int i = 0; i < 3; ++i)
			for (int j = 0; j < 3; ++j)
				if (g_gameboard[h].board_element[i][j] != SGN_EMPTY)
					++filled_fields;
	if (filled_fields == 9 *  3 * 3)
	{
		g_gamestate = GS_END_DRAW;
		return true;
	}

	//---zmiana aktualnej planszy i aktualnego gracza po wykonaniu ruchu
	g_current_field = static_cast<CURRENTFIELD>(field_number - 1);
	g_current_player = (g_current_player == SGN_CIRCLE ? SGN_CROSS : SGN_CIRCLE);
	if (g_vs_computer == true) 
		g_computer_move = (g_computer_move == P_HUMAN ? P_COMPUTER : P_HUMAN);
	++g_number_of_moves;

	return true;
}
//----------------------------------------------------------------------------------------
int ComputerValue()
{
	if (g_gamestate != GS_PLAY) return false;

	const int WINNING_LINES[8][3][2] = {
		{ { 0, 0 },{ 0, 1 },{ 0, 2 } },	// g�rna pozioma
		{ { 1, 0 },{ 1, 1 },{ 1, 2 } },	// �rodkowa pozioma
		{ { 2, 0 },{ 2, 1 },{ 2, 2 } },	// dolna pozioma
		{ { 0, 0 },{ 1, 0 },{ 2, 0 } },	// lewa pionowa
		{ { 0, 1 },{ 1, 1 },{ 2, 1 } },	// �rodkowa pionowa
		{ { 0, 2 },{ 1, 2 },{ 2, 2 } },	// prawa pionowa
		{ { 0, 0 },{ 1, 1 },{ 2, 2 } },	// przek�tna lewo>prawo
		{ { 2, 0 },{ 1, 1 },{ 0, 2 } } };	// przek�tna prawo<lewo
	int field_number, good_movement_chance;
	Board test_gameboard[9];

	//---Losowanie liczb---
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_int_distribution<short> fn_dist(1, 9);
	std::uniform_int_distribution<short> gmc_dist(0, 100);
	field_number = fn_dist(mt); //losowy numer planszy
	good_movement_chance = gmc_dist(mt); //szansa na postawienie ruchu wygrywajacego/blokujacego wygrana

	if (good_movement_chance <= 90) 
	{
		for (int g = 0; g <= 1; ++g) //priorytet wybrania miejsca (najpierw rozpatrzony jest g=0 - wygrana, potem g=1 - blok)
		{
			for (int h = 1; h < 9; ++h)
			{
				//---kopiowanie planszy do testowej struktury---
				for (int i = 0; i < 9; ++i)
					for (int j = 0; j < 3; ++j)
						for (int k = 0; k < 3; ++k)
							test_gameboard[i].board_element[j][k] = g_gameboard[i].board_element[j][k];

				//---przeksztalcenie numerow pol na wspolrzedne X i Y
				int y_coordinate = (h - 1) / 3;
				int x_coordinate = (h - 1) % 3;

				//---warunek sprawdzajacy czy pole jest puste (nie mozna dodac dwa razy tego samego znaku w jedno pole)
				if (test_gameboard[g_current_field].board_element[y_coordinate][x_coordinate] == SGN_EMPTY)
				{
					//---ustawienie odpowiedniego znaczka(x/o) do testowania warunku zwyciestwa---
					if (g_current_player == SGN_CIRCLE)
						test_gameboard[g_current_field].board_element[y_coordinate][x_coordinate] = (g == 0 ? SGN_CIRCLE : SGN_CROSS);
					else
						test_gameboard[g_current_field].board_element[y_coordinate][x_coordinate] = (g == 1 ? SGN_CIRCLE : SGN_CROSS);
				}
				else
					continue;

				//---sprawdzanie warunku zwyciestwa/zablokowania
				for (int i = 0; i < 8; ++i)
				{
					int same_fields_counter = 0;
					SIGN field = SGN_EMPTY;
					SIGN same_field = SGN_EMPTY;

					for (int j = 0; j < 3; ++j)
					{
						field = test_gameboard[g_current_field].board_element[WINNING_LINES[i][j][0]][WINNING_LINES[i][j][1]];
						if (field == same_field)
						{
							++same_fields_counter;
						}
						else
						{
							same_field = field;
							same_fields_counter = 1;
						}
					}

					//---jezeli zajete beda 3 nie puste pola w jednej linii->ustaw znaczek w odpowiednie miejsce
					if (same_fields_counter == 3 && same_field != SGN_EMPTY)
					{
						field_number = h;
						break;
					}
				}
			}
		}
	}
	return field_number;
}
//----------------------------------------------------------------------------------------
bool GameProgress()
{
	int field_number;

	for (;;)
	{
		StartGame();
		for (;;)
		{
			DrawBoard();

			if (g_gamestate == GS_PLAY)
			{
				if (g_computer_move == P_HUMAN)
					field_number = GetValue("Wybierz numer pola: ", 1, 9, false);
				else
					field_number = ComputerValue();
				PlaceSign(field_number);
			}
			else if (g_gamestate == GS_END_WIN || g_gamestate == GS_END_DRAW)
			{
				int menu_option;
				menu_option = GetValue("0 - Powrot do menu, 1 - Nowa Gra: ", 0, 1, false);
				if (menu_option == 0)
				{
					return false;
					break;
				}
				else
				{
					g_gamestate = GS_BEGIN;
					break;
				}
			}
		}
	}
	return true;
}