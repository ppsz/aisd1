#pragma once
#include <random>
#include <limits>
#include <iostream>
#include "getvalue.h"

enum SIGN { SGN_EMPTY = 0, SGN_CIRCLE = 'O', SGN_CROSS = 'X' };

enum GAMESTATE { GS_BEGIN, GS_PLAY, GS_END_WIN, GS_END_DRAW };

enum CURRENTFIELD 
{
	CF_TOP_LEFT=0, CF_TOP=1, CF_TOP_RIGHT=2,
	CF_LEFT=3, CF_CENTER=4, CF_RIGHT=5,
	CF_BOT_LEFT=6, CF_BOT=7, CF_BOT_RIGHT=8
};

enum PLAYER
{
	P_COMPUTER = 0, P_HUMAN = 1
};

struct Board
{
	SIGN board_element[3][3] = { { SGN_EMPTY, SGN_EMPTY, SGN_EMPTY },
								 { SGN_EMPTY, SGN_EMPTY, SGN_EMPTY },
								 { SGN_EMPTY, SGN_EMPTY, SGN_EMPTY } };
};

//extern GAMESTATE g_gamestate;
//extern PLAYER g_computer_move;
extern bool g_vs_computer;

//---Ustawienie warunkow poczatkowych gry---
bool StartGame();
//---Rysowanie planszy--
bool DrawBoard();
//---Postawienie kolka/krzyzyka i sprawdzenie czy wykonany ruch byl wygrywajacy---
bool PlaceSign(int field_number);
//---Prosty algorytm sztucznej inteligencji---
int ComputerValue();
//---Glowna petla gry---
bool GameProgress();